#!/bin/sh

set -e

export DEBIAN_FRONTEND="noninteractive"

# -----------------------------------------------------------------------------
# Build dependencies
# -----------------------------------------------------------------------------
# Installs some needed dependencies with system package manager.
# Freeze versions for packages listed in docker/scripts/spack.config.sh

# update package list
apt-get update 

apt-get install -y tzdata
ln -fs /usr/share/zoneinfo/America/New_York /etc/localtime
dpkg-reconfigure --frontend noninteractive tzdata

# build dependencies
apt-get install -y --no-install-recommends \
        build-essential \
        ca-certificates \
        chrpath \
        curl \
        git \

# python packages 
apt-get install -y --no-install-recommends python3-dev python3-pip python3-setuptools

# thallium build dependencies
apt-get install -y --no-install-recommends \
        autoconf=2.69* \
        automake=1:1.16.1* \
        libboost-dev=1.71.0* \
        libcereal-dev=1.3.0* \
        libjson-c-dev=0.13.1* \
        libpkgconf-dev=1.6.3* \
        libsigsegv-dev=2.12* \
        libtool=2.4.6* \
        m4=1.4.18*  \
        pkg-config

# nasm build dependencies
apt-get install -y --no-install-recommends \
        nasm \
        openssl \
        perl

# update default python version
update-alternatives --install /usr/bin/python python /usr/bin/python3 1
