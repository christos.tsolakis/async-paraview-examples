#!/bin/sh

export PYTHONPATH="${PARAVIEW_ASYNC_RUNTIME_DIR}/lib/python3.8/site-packages"
export PATH="${PARAVIEW_ASYNC_RUNTIME_DIR}/bin:${PATH}"

pvpython \
  $@ \
  -D ${PARAVIEW_ASYNC_DATA_DIR} \
  --server \
  --host 127.0.0.1 \
  --venv ${PARAVIEW_ASYNC_VENV_DIR}
