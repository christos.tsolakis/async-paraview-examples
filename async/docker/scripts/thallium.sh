#!/bin/sh

set -e

readonly spackroot="/opt/spack"

# Clone Spack itself.
git clone --depth 1 https://github.com/spack/spack "$spackroot/src"
export SPACK_ROOT="$spackroot/src"
export PATH="$PATH:$SPACK_ROOT/bin"

# Search for the compiler.
spack compiler find

# Clone mochi package repository.
git clone --depth 1 https://github.com/mochi-hpc/mochi-spack-packages "$spackroot/mochi"

# Add mochi repo to Spack.
spack repo add "$spackroot/mochi"

# Inject distro build tools to avoid unnecessary builds from spack.
mkdir -p "${spackroot}/config"
cat > "${spackroot}/config/packages.yaml" <<EOF
packages:
  all:
    target: [x86_64]
  autoconf:
    buildable: false
    externals:
    - spec: autoconf@2.69
      prefix: /usr
  automake:
    buildable: false
    externals:
    - spec: automake@1.16.1
      prefix: /usr
  boost:
    buildable: false
    externals:
    - spec: boost@1.71.0
      prefix: /usr
  cereal:
    buildable: false
    externals:
    - spec: cereal@1.3.0
      prefix: /usr
  cmake:
    buildable: false
    externals:
    - spec: cmake@$( cmake --version | head -n1 | cut -d' ' -f3 )
      prefix: /opt/cmake
  json-c:
    buildable: false
    externals:
    - spec: json-c@0.13.1
      prefix: /usr
  libtool:
    buildable: false
    externals:
    - spec: libtool@2.4.6
      prefix: /usr
  libsigsegv:
    buildable: false
    externals:
    - spec: libsigsegv@2.12
      prefix: /usr
  m4:
    buildable: false
    externals:
    - spec: m4@1.4.18
      prefix: /usr
  perl:
    buildable: false
    externals:
    - spec: perl@5.30.0
      prefix: /usr
  pkgconf:
    buildable: false
    externals:
    - spec: pkgconf@1.6.3
      prefix: /usr
EOF

readonly spec="mochi-thallium@0.10.1 ^libfabric fabrics=tcp,rxm,sockets ^mochi-margo@0.9.10 ^mercury@2.2.0 ^argobots@dce6e727ffc4ca5b3ffc04cb9517c6689be51ec5=main"
# inspect spec
spack -C "$spackroot/config" spec $spec

# Install thallium.
spack -C "$spackroot/config" install $spec

# Clean up the directory.
spack -C "$spackroot/config" clean
