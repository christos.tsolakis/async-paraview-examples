#!/bin/sh

set -e

export DEBIAN_FRONTEND="noninteractive"
# -----------------------------------------------------------------------------
# System package
# -----------------------------------------------------------------------------
apt-get update 

apt-get install -y tzdata
ln -fs /usr/share/zoneinfo/America/New_York /etc/localtime
dpkg-reconfigure --frontend noninteractive tzdata

apt-get install -y --no-install-recommends \
    ca-certificates \
    libatomic1 \
    libjson-c-dev=0.13.1+dfsg-7ubuntu0.3 \
    libosmesa6 \
    libsigsegv-dev=2.12-2 \
    openssl

apt-get install -y --no-install-recommends python3-dev python3.8-venv python3-pip python3-setuptools
update-alternatives --install /usr/bin/python python /usr/bin/python3 1 

#rm -rf /var/lib/apt/lists/*
