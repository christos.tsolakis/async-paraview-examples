## Building docker image locally

For reproducibility and comparison with the synchronous version you may also create
your own docker image. Two base images are provided. One based on nvidia EGL
that uses hardware accelerated rendering as well as a software-based rendering
using osmesa.

### NVIDIA EGL rendering

You may need to setup `nvidia-docker2`. Follow instructions outlined [here](https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/install-guide.html).

```bash
$ ./async/docker/nvidia/build-all.sh
```

### OSMesa software rendering
```bash
$ ./async/docker/osmesa/build-all.sh
```

To run the any of the reference applications use
```bash
$ ./async/docker/osmesa/run-docker.sh <python script> <script arguments>
```

## Generated image

The docker runtime containers have everything you need to run `wavelet.py`
and `rock.py`

Here's a listing of `/opt/paraview`

```bash
/opt/paraview/
|-- apps
|   |-- rock.py
|   `-- wavelet.py
|-- data
|   `-- Testing
`-- runtime
    |-- .pvenv
    |-- bin
    |-- lib
    `-- share
```
