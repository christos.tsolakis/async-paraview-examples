#!/usr/bin/env bash
DATE=`date "+%Y_%m_%d"`
docker save paraview-async-osmesa-run | gzip > "paraview-async-osmesa-run-$DATE.tgz"
