ARG BASE_IMAGE=ubuntu:20.04
FROM ${BASE_IMAGE}

LABEL maintainer="Jaswant Panchumarti <jaswant.panchumarti@kitware.com>"

ENV TZ "America/New_York"

# Not necessary to mkdir. only serves as a list of dirs we touch.
RUN mkdir -p \
    /opt \
    /opt/paraview-async/build \
    /opt/paraview-async/src \
    /opt/paraview-async/install \
    /opt/scripts \
    /opt/spack \
    /opt/mochi-spackages

WORKDIR /opt

COPY docker/scripts/install_base_dependencies.sh /opt/install_base_dependencies.sh
RUN /opt/install_base_dependencies.sh

# Get cmake
COPY docker/scripts/cmake.sh /opt/cmake.sh
RUN /opt/cmake.sh
ENV PATH="/opt/cmake/bin:${PATH}"

# Get ninja
COPY docker/scripts/ninja.sh /opt/ninja.sh
RUN /opt/ninja.sh
ENV PATH="/opt/bin:${PATH}"

# -----------------------------------------------------------------------------
# Spack + Thallium
# -----------------------------------------------------------------------------
COPY docker/scripts/thallium.sh /opt/thallium.sh
RUN /opt/thallium.sh
ENV PATH="/opt/spack/bin:${PATH}"

# add osmesa specific packages
RUN DEBIAN_FRONTEND="noninteractive" apt-get install libosmesa6-dev -y

# -----------------------------------------------------------------------------
# ParaView Async Project
# -----------------------------------------------------------------------------
# Gets the project from git repo.
COPY docker/scripts/clone_paraview_async.sh /opt/clone_paraview_async.sh
RUN /opt/clone_paraview_async.sh

# Configures the project with cmake.
COPY docker/scripts/configure_common.cmake /opt/configure_common.cmake

RUN . spack/src/share/spack/setup-env.sh && \
  spack load mochi-thallium && \
  cmake -GNinja \
  -S paraview-async/src \
  -B paraview-async/build \
  -C /opt/configure_common.cmake \
  -D VTK_OPENGL_HAS_OSMESA=ON

# Builds the project with ninja.
RUN . spack/src/share/spack/setup-env.sh && \
  spack load mochi-thallium && \
  cmake --build paraview-async/build

# Install the project.
RUN . spack/src/share/spack/setup-env.sh && \
  spack load mochi-thallium && \
  cmake --install paraview-async/build --prefix paraview-async/install

ENTRYPOINT [ "/bin/bash" ]
