#!/usr/bin/env bash
DATE=`date "+%Y_%m_%d"`
docker save paraview-async-run | gzip > "paraview-async-run-$DATE.tgz"
