#!/usr/bin/env bash
#
# ./run-docker.sh <python script> <script arguments>
# Ex: ./run-docker.sh rock.py --port 8080
#
#if the local image does not exist use the one from dockerhub
IMAGE=paraview-async-run:latest
docker image inspect $IMAGE > /dev/null

if [ $? -eq 1 ]; then
  IMAGE=kitware/paraview:async-demo-SC22
fi

docker run \
  -it \
  --gpus all \
  --network="host" \
  -e NVIDIA_DRIVER_CAPABILITIES=all \
  -w "/root" \
  -v "$(pwd):/root/" \
  ${IMAGE} $@
