# -----------------------------------------------------------------------------
# export PYTHONPATH==/path/to/async/paraview/build/lib/pythonx.y/site-packages
# export LD_LIBRARY_PATH==/path/to/async/paraview/build/lib
#
# python3.10 -m venv pv-venv
# source ./pv-venv/bin/activate
# pip install -U pip
# pip install -U trame
#
# python ./rock.py -D <async-paraview build directory>/ExternalData 
# or using apvpython:
# apvpython ./rock.py --venv ./pv-venv
# -----------------------------------------------------------------------------

from vtkmodules.vtkCommonCore import vtkLogger
import async_paraview.venv
import asyncio
import json
from async_paraview.services import (
    ApplyController,
    ParaT,
    PipelineBuilder,
    DefinitionManager,
    ActiveObjects,
    PropertyManager,
    PipelineViewer,
    ProgressObserver,
)
from async_paraview.modules.vtkRemotingServerManager import vtkSMProxySelectionModel
from async_paraview.trame.rca import ViewAdapter

from trame.app import get_server, asynchronous
from trame.ui.vuetify import SinglePageWithDrawerLayout
from trame.widgets import vuetify, trame, rca, html

# -----------------------------------------------------------------------------
# Global helpers
# -----------------------------------------------------------------------------

vtkLogger.SetStderrVerbosity(vtkLogger.VERBOSITY_WARNING)
from vtk.util.misc import vtkGetDataRoot
import os.path

# Parse -D argument
PATHS = [os.path.join(vtkGetDataRoot(), "Testing/Data/rock.vti"), os.path.join(vtkGetDataRoot(), "rock.vti")]
FILENAME = None
for path in PATHS:
    if not os.path.exists(path):
      continue
    FILENAME = path

ROCK_SCALAR_RANGE = [0, 255]
ROCK_BOUNDS = [0, 354, 0, 354, 0, 405]
BITRATE_UNITS = ["B/s", "KB/s", "MB/s"]
INITIAL_STREAM_BITRATE = 12000
INITIAL_STILL_STREAM_QUALITY = 90
INITIAL_INTERACTIVE_STREAM_QUALITY = 60
INITIAL_TARGET_FPS = 30 # suggested for jpeg.


def scale_data_rate_to_bytes(value_in_bits):
    return value_in_bits / 8


# value must be in bits/sec
def get_human_readable_stream_bitrate(value_in_bps):
    value = scale_data_rate_to_bytes(value_in_bps)
    for i, units in enumerate(BITRATE_UNITS):
        if value < 1000:
            return f"{value} {units}"
        else:
            value /= 1000


# -----------------------------------------------------------------------------
# Core Application
# -----------------------------------------------------------------------------
class App:
    def __init__(self, server=None):
        if server is None:
            server = get_server()

        self.server = server
        self.state = server.state
        self.ctrl = server.controller

        self.state.trame__title = "Async ParaView - Rock Dataset"

        # ParaView Async
        self._app = ParaT()
        self._running = True
        self._ready = False

        # internal state
        self.hidden_pipeline_proxy_ids = []
        self.active_proxy = None
        self.active_representation = None
        self.active_view = None

        # initial state
        self.state.target_fps = INITIAL_TARGET_FPS
        self.state.readable_stream_bitrate = get_human_readable_stream_bitrate(
            INITIAL_STREAM_BITRATE * 1000
        )
        self.state.contour_value = 187
        self.state.working_proxy = " "
        self.state.active_data_info = {"points": 0, "cells": 0}
        self.state.reader_id = 0
        self.state.contour_id = 0
        self.state.clip_id = 0
        self.state.slice_id = 0
        self.state.threshold_id = 0
        self.state.resample_id = 0

        # controller
        self.ctrl.on_server_ready.add_task(self.initialize)
        self.ctrl.on_server_exited.add_task(self.finalize)

        # state listeners
        self.state.change("contour_value")(self.ui_state_contours_update)
        self.state.change("clip_x_origin")(self.ui_state_clip_update)
        self.state.change("slice_x_origin")(self.ui_state_slice_update)
        self.state.change("stream_encoder")(self.ui_state_encoder_update)
        self.state.change("threshold_range")(self.ui_state_threshold_update)
        self.state.change("resample_value")(self.ui_state_resample_update)
        self.state.change("target_fps")(self.ui_state_target_fps_change)
        self.state.change("stream_quality_range")(
            self.ui_state_stream_quality_range_change
        )
        self.state.change("stream_bitrate")(self.ui_state_stream_bitrate_change)

    # ---------------------------------------------------------------
    # Instance life cycle
    # ---------------------------------------------------------------

    async def initialize(self, **kwargs):
        self._session = await self._app.initialize()
        self._pipeline = PipelineViewer(self._session)
        self._builder = PipelineBuilder(self._session)
        self._def_mgt = DefinitionManager(self._session)
        self._active = ActiveObjects(self._session, "ActiveSources")
        self._prop_mgr = PropertyManager()
        self._progress = ProgressObserver(self._session)
        self._apply_ctrl = ApplyController(self._session)

        # default demo pipeline
        await self.setup_demo()

        # Tasks to monitor state change
        asynchronous.create_task(self.monitor_active_proxy())
        asynchronous.create_task(self.monitor_apply())
        asynchronous.create_task(self.monitor_pipeline_update())
        asynchronous.create_task(self.monitor_progress_ds())
        asynchronous.create_task(self.monitor_progress_rs())
        asynchronous.create_task(self.monitor_server_status())

        # RemoteControllerArea
        self._view_handler = ViewAdapter(self.active_view, "view")
        self.ctrl.rc_area_register(self._view_handler)

        self._ready = True

    async def finalize(self, **kwargs):
        await self._app.close(self._session)

    # ---------------------------------------------------------------
    # Background async monitoring tasks
    # ---------------------------------------------------------------

    async def monitor_active_proxy(self):
        async for proxy in self._active.GetCurrentObservable():
            self.active_proxy = proxy
            active_ids = []
            data_info = {}

            if proxy:
                active_ids = [str(proxy.GetGlobalID())]
                info = proxy.GetDataInformation(0)
                data_info = dict(
                    points=info.GetNumberOfPoints(), cells=info.GetNumberOfCells()
                )

            with self.state as state:
                state.git_tree_actives = active_ids
                state.active_data_info = data_info

    async def monitor_apply(self):
        async for apply_event in self._apply_ctrl.GetObservable():
            if self.active_proxy:
                await self._prop_mgr.UpdatePipeline(self.active_proxy)
                with self.state as state:
                    info = self.active_proxy.GetDataInformation(0)
                    state.active_data_info = dict(
                        points=info.GetNumberOfPoints(), cells=info.GetNumberOfCells()
                    )

    async def monitor_pipeline_update(self):
        async for pipelineState in self._pipeline.GetObservable():
            self._push_pipeline_to_ui(pipelineState)

    async def monitor_progress_ds(self):
        async for message in self._progress.GetServiceProgressObservable("ds"):
            data = json.loads(message)
            message = data.get("ds", {})
            progress = message.get("Progress", 100)

            with self.state as state:
                state.status_ds_idle = progress == 100
                state.status_ds_progress = progress
                if state.status_ds_idle:
                    self.state.working_proxy = " "
                else:
                    self.state.working_proxy = message.get("Message", "")

    async def monitor_progress_rs(self):
        async for message in self._progress.GetServiceProgressObservable("rs"):
            data = json.loads(message)
            progress = data.get("rs", {}).get("Progress", 100)
            self.state.status_rs_idle = progress == 100

    async def monitor_server_status(self):
        while self._running:
            with self.state as state:
                await asyncio.sleep(1 / self.state.target_fps)

                # Spinning
                if state.spinning and self.active_view:
                    self.active_view.GetCamera().Azimuth(1)
                    self.active_view.InteractiveRender()

                # Update client
                state.status_server += 5
                if state.status_server > 360:
                    state.status_server = 0

    # ---------------------------------------------------------------
    # General API
    # ---------------------------------------------------------------

    async def create_proxy(self):
        with self.state as state:
            xml_group = state.xml_group
            xml_name = state.xml_name
            proxy = None
            if xml_group == "sources":
                proxy = await self._builder.CreateProxy(xml_group, xml_name)
            elif xml_group == "filters":
                input = self.active_proxy
                proxy = await self._builder.CreateProxy(
                    xml_group, xml_name, Input=input
                )
            elif xml_group == "representations":
                input = self.active_proxy
                view = self.active_view
                proxy = await self._builder.CreateRepresentation(
                    input, 0, view, xml_name
                )
                self.active_representation = proxy
            elif xml_group == "views":
                input = self.active_proxy
                proxy = await self._builder.CreateProxy(xml_group, xml_name)
                self.active_view = proxy
            else:
                print(f"Not sure what to create with {xml_group}::{xml_name}")

            # No proxy just skip work...
            if proxy is None:
                print("!!! No proxy created !!!")
                return

    async def setup_demo(self):
        view = await self._builder.CreateProxy("views", "RenderView")
        hide_list = []

        minX, maxX, minY, maxY, minZ, maxZ = ROCK_BOUNDS
        midX = (maxX - minX) * 0.5
        midY = (maxY - minY) * 0.5
        midZ = (maxZ - minZ) * 0.5

        # reader ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        # creating a representation before Apply() we avoid the default type
        # which is volume representation
        self.reader = await self._builder.CreateProxy(
            "sources", "XMLImageDataReader", FileName=FILENAME
        )
        rep = await self._builder.CreateRepresentation(
            self.reader,
            0,
            view,
            "GeometryRepresentation",
        )
        self._prop_mgr.SetValues(rep, Representation="Outline")
        self._prop_mgr.Push(self.reader, rep)

        self.resample = await self._builder.CreateProxy(
            "filters",
            "ResampleToImage",
            Input=self.reader,
            SamplingDimensions=[300, 300, 300],
            SamplingBounds=[0, 354, 0, 354, 0, 405],
        )
        rep = await self._builder.CreateRepresentation(
            self.resample, 0, view, "GeometryRepresentation"
        )
        self._prop_mgr.SetValues(rep, Representation="Outline")
        self._prop_mgr.Push(self.resample, rep)

        # Contour ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        self.contour = await self._builder.CreateProxy(
            "filters",
            "Contour",
            Input=self.resample,
            ContourValues=[187],
            SelectInputScalars=["", "", "", "0", "scalars"],
        )
        rep = await self._builder.CreateRepresentation(
            self.contour, 0, view, "GeometryRepresentation"
        )
        self._prop_mgr.Push(self.contour, rep)
        hide_list.append(rep)

        # Clip ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        self.clip = await self._builder.CreateProxy(
            "filters",
            "Clip",
            Input=self.contour,
            PreserveInputCells=True,  # aka Crinkle clip
        )
        rep = await self._builder.CreateRepresentation(
            self.clip, 0, view, "GeometryRepresentation"
        )
        clipFunction = self._prop_mgr.GetValues(self.clip)["ClipFunction"]
        self._prop_mgr.SetValues(
            clipFunction,
            force_push=True,
            Normal=[1, 0, 0],
            Origin=[0, 0, 0],
            Offset=midX,
        )
        self._prop_mgr.SetValues(self.clip, ClipFunction=clipFunction)
        self._prop_mgr.Push(self.clip, rep)

        # Slice ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        self.slice = await self._builder.CreateProxy(
            "filters",
            "Cut",
            Input=self.resample,
        )
        cutFunction = self._prop_mgr.GetValues(self.slice)["CutFunction"]
        self._prop_mgr.SetValues(
            cutFunction,
            force_push=True,
            Normal=[0, 1, 0],
            Origin=[0, 0, 0],
            Offset=midX,
        )
        self._prop_mgr.SetValues(self.slice, CutFunction=cutFunction)

        rep = await self._builder.CreateRepresentation(
            self.slice, 0, view, "GeometryRepresentation"
        )
        self._prop_mgr.Push(self.slice, rep)

        # Threshold ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        self.threshold = await self._builder.CreateProxy(
            "filters",
            "Threshold",
            Input=self.resample,
            SelectInputScalars=["", "", "", "", "scalars"],
            LowerThreshold=100,
            UpperThreshold=130,
        )
        rep = await self._builder.CreateRepresentation(
            self.threshold, 0, view, "GeometryRepresentation"
        )
        self._prop_mgr.Push(self.threshold, rep)

        # Plane ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        # create a plane source as a fake widget of the clip
        # make sure to keep them synchronized

        self.plane = await self._builder.CreateProxy(
            "sources",
            "PlaneSource",
            Origin=[midX, minY, minZ],
            Point1=[midX, minY, maxZ],
            Point2=[midX, maxY, minZ],
            XResolution=10,
            YResolution=10,
        )
        rep = await self._builder.CreateRepresentation(
            self.plane, 0, view, "GeometryRepresentation"
        )
        self._prop_mgr.SetValues(rep, Representation="Wireframe")
        self._prop_mgr.Push(self.plane, rep)
        self.hidden_pipeline_proxy_ids.append(self.plane.GetGlobalID())

        # View encoding setup
        self._prop_mgr.SetValues(
            view,
            force_push=True,
            LosslessMode=False,
            JpegMode=True,
            StillStreamQuality=INITIAL_STILL_STREAM_QUALITY,
            InteractiveStreamQuality=INITIAL_INTERACTIVE_STREAM_QUALITY,
            TargetBitrate=INITIAL_STREAM_BITRATE,
            Display=False,
            StreamOutput=True,
        )
        # Push everything after view is setup
        self._apply_ctrl.Apply()
        # ideally we would like to wait on the internal update of Apply()
        await self._prop_mgr.Update(view)

        # Visibility handling (need to happen after apply)
        for rep in hide_list:
            self._prop_mgr.SetValues(
                rep,
                force_push=True,
                Visibility=0,
            )

        # Keep track of view and ids
        self.active_view = view
        self.state.reader_id = str(self.reader.GetGlobalID())
        self.state.contour_id = str(self.contour.GetGlobalID())
        self.state.clip_id = str(self.clip.GetGlobalID())
        self.state.slice_id = str(self.slice.GetGlobalID())
        self.state.resample_id = str(self.resample.GetGlobalID())
        self.state.threshold_id = str(self.threshold.GetGlobalID())

        # Activate reader by default
        self._active.SetCurrentProxy(self.reader, vtkSMProxySelectionModel.CLEAR)

    def _push_pipeline_to_ui(self, pipelineState):
        list_to_fill = []
        for item in pipelineState:
            if self.active_view is None:
              visible = False
            else:
              rep_id = item.GetRepresentationIDs(self.active_view)[0]
              rep = self._session.GetProxyManager().FindProxy(rep_id)
              visible = self._prop_mgr.GetValues(rep)["Visibility"] if rep else False
            node = {
                "name": item.GetName(),
                "parent": str(
                    item.GetParentIDs()[0] if len(item.GetParentIDs()) > 0 else 0
                ),
                "id": str(item.GetID()),
                "visible": visible,
            }
            if item.GetID() not in self.hidden_pipeline_proxy_ids:
                list_to_fill.append(node)

        with self.state as state:
            state.git_tree_sources = list_to_fill

    # ---------------------------------------------------------------
    # GUI callbacks
    # ---------------------------------------------------------------

    def ui_event_pipeline_update(self, active):
        proxy = self._session.GetProxyManager().FindProxy(int(active[0]))
        self._active.SetCurrentProxy(proxy, vtkSMProxySelectionModel.CLEAR)

    def ui_event_pipeline_visibility_update(self, selection):
        proxy_id = selection.get("id")
        visible = selection.get("visible")
        proxy = self._session.GetProxyManager().FindProxy(int(proxy_id))
        rep = self.active_view.FindRepresentation(proxy, 0)
        self._prop_mgr.SetValues(rep, force_push=True, Visibility=visible)

        # update visibility also for the cutting plane
        if proxy == self.clip:
            rep = self.active_view.FindRepresentation(self.plane, 0)
            self._prop_mgr.SetValues(rep, force_push=True, Visibility=visible)

        self._apply_ctrl.Apply()

        pipelineState = self._pipeline.GetCurrentState()
        self._push_pipeline_to_ui(pipelineState)

    def ui_state_resample_update(self, resample_value, **kwargs):
        if not self._ready:
            return

        self._prop_mgr.SetValues(
            self.resample,
            force_push=True,
            SamplingDimensions=[resample_value, resample_value, resample_value],
        )
        self._apply_ctrl.Apply()

    def ui_state_contours_update(self, contour_value, **kwargs):
        if not self._ready:
            return

        self._prop_mgr.SetValues(
            self.contour, force_push=True, ContourValues=[contour_value]
        )
        self._apply_ctrl.Apply()

    def ui_state_clip_update(self, clip_x_origin, **kwargs):
        if not self._ready:
            return

        minX, maxX, minY, maxY, minZ, maxZ = ROCK_BOUNDS
        scaled_offset = (maxX - minX) * clip_x_origin

        # update plane first
        self._prop_mgr.SetValues(
            self.plane,
            force_push=True,
            Origin=[scaled_offset, minY, minZ],
            Point1=[scaled_offset, minY, maxZ],
            Point2=[scaled_offset, maxY, minZ],
        )

        clipFunction = self._prop_mgr.GetValues(self.clip)["ClipFunction"]
        self._prop_mgr.SetValues(clipFunction, force_push=True, Offset=scaled_offset)
        # see  async/paraview#136
        self._prop_mgr.SetValues(self.clip, ClipFunction=clipFunction, force_push=True)
        self.clip.GetProperty("ClipFunction").Modified()

        self._apply_ctrl.Apply()

    def ui_state_slice_update(self, slice_x_origin, **kwargs):
        if not self._ready:
            return

        minX, maxX = ROCK_BOUNDS[0:2]
        scaled_offset = (maxX - minX) * slice_x_origin

        cutFunction = self._prop_mgr.GetValues(self.slice)["CutFunction"]
        self._prop_mgr.SetValues(cutFunction, force_push=True, Offset=scaled_offset)
        # see  async/paraview#136
        self._prop_mgr.SetValues(self.slice, CutFunction=cutFunction, force_push=True)
        self.slice.GetProperty("CutFunction").Modified()

        self._apply_ctrl.Apply()

    def ui_state_threshold_update(self, threshold_range, **kwargs):
        if not self._ready:
            return
        self._prop_mgr.SetValues(
            self.threshold,
            force_push=True,
            LowerThreshold=threshold_range[0],
            UpperThreshold=threshold_range[1],
        )
        self._apply_ctrl.Apply()

    def ui_state_target_fps_change(self, target_fps, **kwargs):
        if not self._ready:
            return
        self._view_handler.target_fps = target_fps

    def ui_state_encoder_update(self, stream_encoder, **kwargs):
        if not self._ready:
            return

        if stream_encoder == "RGB":
            self._prop_mgr.SetValues(
                self.active_view,
                force_push=True,
                JpegMode=False,
                LosslessMode=True,
                Display=False,
                StreamOutput=True,
                LosslessPixelFormat=1,  # VTKPixelFormatType::RGB24
                LosslessPictureSliceOrder=0,  # vtkRawVideoFrame::SliceOrder::TopDown
            )
            self.state.active_display_mode = "raw-image"

        if stream_encoder == "RGBA":
            self._prop_mgr.SetValues(
                self.active_view,
                force_push=True,
                JpegMode=False,
                LosslessMode=True,
                Display=False,
                StreamOutput=True,
                LosslessPixelFormat=0,  # VTKPixelFormatType::RGBA32
                LosslessPictureSliceOrder=0,  # vtkRawVideoFrame::SliceOrder::TopDown
            )
            self.state.active_display_mode = "raw-image"

        if stream_encoder == "JPG":
            self._prop_mgr.SetValues(
                self.active_view,
                force_push=True,
                JpegMode=True,
                LosslessMode=False,
                Display=False,
                StreamOutput=True,
            )
            self.state.active_display_mode = "image"

        # VP9 is the default, so, there is no need to acquire the encoder proxy and set codec type.
        if stream_encoder == "VP9_SW":
            print("Use VP9 with software encoder")
            self._prop_mgr.SetValues(
                self.active_view,
                force_push=True,
                JpegMode=False,
                LosslessMode=False,
                Display=False,
                StreamOutput=True,
                VideoEncoderType="VpxEncoder",
                MediaSegmentsMode=False,
                RequestMediaInitializationSegment=False,
            )
            self.state.active_display_mode = "video-decoder"

        if stream_encoder == "H264_NVENC":
            print("Use H264 with NVENC(Nvidia) hardware encoder")
            self._prop_mgr.SetValues(
                self.active_view,
                force_push=True,
                JpegMode=False,
                LosslessMode=False,
                Display=False,
                StreamOutput=True,
                VideoEncoderType="NVIDIAEncoder",
                MediaSegmentsMode=False,
                RequestMediaInitializationSegment=False,
            )
            self.state.active_display_mode = "video-decoder"

        if stream_encoder == "VP9_SW_WEBM":
            print("Use VP9_WEBM with software encoder")
            self._prop_mgr.SetValues(
                self.active_view,
                force_push=True,
                JpegMode=False,
                LosslessMode=False,
                Display=False,
                StreamOutput=True,
                VideoEncoderType="VpxEncoder",
                MediaSegmentsMode=True,
                RequestMediaInitializationSegment=True,
            )
            self.state.active_display_mode = "media-source"

        self.active_view.StillRender()

    def ui_state_stream_quality_range_change(self, stream_quality_range, **kwargs):
        if not self._ready:
            return

        self._prop_mgr.SetValues(
            self.active_view,
            force_push=True,
            InteractiveStreamQuality=stream_quality_range[0],
            StillStreamQuality=stream_quality_range[1],
        )

    def ui_state_stream_bitrate_change(self, stream_bitrate, **kwargs):
        if not self._ready:
            return

        self._prop_mgr.SetValues(
            self.active_view,
            force_push=True,
            TargetBitrate=stream_bitrate,
        )

        value = stream_bitrate * 1000
        self.state.readable_stream_bitrate = get_human_readable_stream_bitrate(value)

    def reset_target_fps(self):
        if not self._ready:
            return

        self.state.target_fps = INITIAL_TARGET_FPS

    def reset_stream_bitrate(self):
        if not self._ready:
            return

        self.state.stream_bitrate = INITIAL_STREAM_BITRATE
        value = INITIAL_STREAM_BITRATE * 1000
        self.state.readable_stream_bitrate = get_human_readable_stream_bitrate(value)

    def reset_stream_quality(self):
        if not self._ready:
            return

        self.state.stream_quality_range = (INITIAL_INTERACTIVE_STREAM_QUALITY, INITIAL_STILL_STREAM_QUALITY)


# -----------------------------------------------------------------------------
# Setup
# -----------------------------------------------------------------------------

server = get_server()
app = App(server)

# -----------------------------------------------------------------------------
# GUI
# -----------------------------------------------------------------------------

with SinglePageWithDrawerLayout(server) as layout:
    with layout.icon:
        vuetify.VIcon("mdi-clock-fast")

    with layout.title as title:
        title.style = "padding-left: 0;"
        title.set_text("Async ParaView")

    with layout.toolbar as toolbar:
        toolbar.dense = True
        vuetify.VSpacer()
        html.Div("{{ working_proxy }}", classes="text-subtitle-1")
        vuetify.VProgressCircular(
            "D",
            color="amber",
            size=35,
            width=5,
            indeterminate=("status_ds_idle", True),
            value=("status_ds_progress", 20),
            classes="mx-2",
        )
        vuetify.VProgressCircular(
            "R",
            color="purple",
            size=35,
            width=5,
            indeterminate=("status_rs_idle", True),
            rotate=("status_server", 0),
            value=("20",),
            classes="mx-2",
        )
        vuetify.VProgressCircular(
            "S",
            color="red",
            size=35,
            width=5,
            rotate=("status_server", 0),
            value=("20",),
            classes="mx-2",
        )
        vuetify.VProgressCircular(
            "C",
            color="teal",
            size=35,
            width=5,
            indeterminate=True,
            classes="mx-2",
        )
        vuetify.VDivider(vertical=True, classes="mx-2")
        vuetify.VCheckbox(
            small=True,
            v_model=("spinning", False),
            dense=True,
            classes="mx-2",
            hide_details=True,
            color="success",
            on_icon="mdi-axis-z-rotate-counterclockwise",
            off_icon="mdi-axis-z-rotate-counterclockwise",
        )
        with vuetify.VBtn(
            icon=True, small=True, click=app.ctrl.view_reset_camera, classes="mx-2"
        ):
            vuetify.VIcon("mdi-crop-free")

    with layout.drawer as drawer:
        drawer.width = 300
        trame.GitTree(
            sources=("git_tree_sources", []),
            actives=("git_tree_actives", []),
            visibility_change=(app.ui_event_pipeline_visibility_update, "[$event]"),
            actives_change=(app.ui_event_pipeline_update, "[$event]"),
        )
        with vuetify.VCard(
            classes="mb-2 mx-1", v_show="git_tree_actives.includes(resample_id)"
        ):
            with vuetify.VCardTitle(classes="py-0"):
                html.Div("Resample Size")
                vuetify.VSpacer()
                html.Div("{{ resample_value }}")

            vuetify.VDivider()
            with vuetify.VCardText():
                vuetify.VSlider(
                    v_model=("resample_value", 300),
                    min=300,
                    max=500,
                    step=1,
                    hide_details=True,
                    dense=True,
                )
        with vuetify.VCard(
            classes="mb-2 mx-1", v_show="git_tree_actives.includes(contour_id)"
        ):
            with vuetify.VCardTitle(classes="py-0"):
                html.Div("Contour Value")
                vuetify.VSpacer()
                html.Div("{{ contour_value }}")

            vuetify.VDivider()
            with vuetify.VCardText():
                vuetify.VSlider(
                    v_model=("contour_value", 187),
                    min=1,
                    max=255,
                    step=1,
                    hide_details=True,
                    dense=True,
                )
        with vuetify.VCard(
            classes="mb-2 mx-1", v_show="git_tree_actives.includes(clip_id)"
        ):
            with vuetify.VCardTitle(classes="py-0"):
                html.Div("Clip")
                vuetify.VSpacer()
                html.Div("{{ clip_x_origin }}")

            vuetify.VDivider()
            with vuetify.VCardText():
                vuetify.VSlider(
                    v_model=("clip_x_origin", 0.5),
                    min=0,
                    max=1,
                    step=0.1,
                    hide_details=True,
                    dense=True,
                )
        with vuetify.VCard(
            classes="mb-2 mx-1", v_show="git_tree_actives.includes(slice_id)"
        ):
            with vuetify.VCardTitle(classes="py-0"):
                html.Div("Slice")
                vuetify.VSpacer()
                html.Div("{{ slice_x_origin }}")

            vuetify.VDivider()
            with vuetify.VCardText():
                vuetify.VSlider(
                    v_model=("slice_x_origin", 0.5),
                    min=0,
                    max=1,
                    step=0.1,
                    hide_details=True,
                    dense=True,
                )
        with vuetify.VCard(
            classes="mb-2 mx-1", v_show="git_tree_actives.includes(threshold_id)"
        ):
            with vuetify.VCardTitle(classes="py-0"):
                html.Div("Threshold")
                vuetify.VSpacer()
                html.Div("{{ threshold_range }}")
            vuetify.VDivider()
            with vuetify.VCardText():
                vuetify.VRangeSlider(
                    v_model=("threshold_range", (100, 130)),
                    min=ROCK_SCALAR_RANGE[0],
                    max=ROCK_SCALAR_RANGE[1],
                    step=1,
                    hide_details=True,
                    dense=True,
                )

        with vuetify.VCard(classes="mb-2 mx-1"):
            with vuetify.VCardTitle(classes="py-0"):
                html.Div("Stats")
                vuetify.VSpacer()
                vuetify.VIcon("mdi-dots-triangle", x_small=True, classes="mr-1")
                html.Div(
                    "{{ active_data_info?.points.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',') | 0 }}",
                    classes="text-caption",
                )
                vuetify.VSpacer()
                vuetify.VIcon("mdi-triangle-outline", x_small=True, classes="mr-1")
                html.Div(
                    "{{ active_data_info?.cells.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',') || 0 }}",
                    classes="text-caption",
                )
            vuetify.VDivider()
            with vuetify.VCardText(style="height: 150px"):
                rca.StatisticsDisplay(
                    name="view",
                    fps_delta=1.5,
                    stat_window_size=10,
                    history_window_size=30,
                    reset_ms_threshold=100,
                )

        with vuetify.VCard(classes="my-2 mx-1"):
            with vuetify.VCardTitle(classes="py-0"):
                html.Div("Image Delivery")
            vuetify.VDivider()
            with vuetify.VCardText():
                vuetify.VSelect(
                    label="Encoder",
                    v_model=("stream_encoder", "JPG"),
                    items=(
                        "['RGB', 'RGBA', 'JPG', 'VP9_SW', 'H264_NVENC', 'VP9_SW_WEBM']",
                    ),
                    hide_details=True,
                    dense=True,
                )
                html.Div("Target: {{ target_fps }} fps", classes="text-subtitle-2 mt-4")
                with vuetify.VRow():
                    vuetify.VSlider(
                        v_model=("target_fps", 30),
                        min=10,
                        max=60,
                        step=5,
                        hide_details=True,
                        dense=True,
                    )
                    with vuetify.VBtn(
                        icon=True, small=True, click=app.reset_target_fps, classes="mx-2"
                    ):
                        vuetify.VIcon("mdi-restore")
                with html.Div(
                    v_show="active_display_mode != 'image' && active_display_mode != 'raw-image'"
                ):
                    html.Div(
                        "Target bitrate: {{ readable_stream_bitrate }}",
                        classes="text-subtitle-2 mt-4",
                    )
                    with vuetify.VRow():
                        vuetify.VSlider(
                            v_model=("stream_bitrate", INITIAL_STREAM_BITRATE),
                            min=100,
                            max=16000,
                            step=400,
                            hide_details=True,
                            dense=True,
                        )
                        with vuetify.VBtn(
                            icon=True, small=True, click=app.reset_stream_bitrate, classes="mx-2"
                        ):
                            vuetify.VIcon("mdi-restore")
                with html.Div(v_show="active_display_mode != 'raw-image'"):
                    html.Div(
                        "Stream quality: {{ stream_quality_range }}",
                        classes="text-subtitle-2 mt-4",
                    )
                    with vuetify.VRow():
                        vuetify.VRangeSlider(
                            v_model=("stream_quality_range", (INITIAL_INTERACTIVE_STREAM_QUALITY, INITIAL_STILL_STREAM_QUALITY)),
                            min=0,
                            max=100,
                            step=1,
                            hide_details=True,
                            dense=True,
                        )
                        with vuetify.VBtn(
                            icon=True, small=True, left=False,  click=app.reset_stream_quality, classes="mx-2"
                        ):
                            vuetify.VIcon("mdi-restore")

    with layout.content:
        with vuetify.VContainer(fluid=True, classes="pa-0 fill-height"):
            rca.RemoteControlledArea(
                name="view", display=("active_display_mode", "image")
            )

# -----------------------------------------------------------------------------
# CLI
# -----------------------------------------------------------------------------

if __name__ == "__main__":
    server.start()
