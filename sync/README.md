# Running the reference examples with the regular ParaView backend as comparison

The runtime environment for the Synchronous backend can be setup either by using a recent (5.11) ParaView binary and a virtual environment or using docker.

## Using ParaView binaries

- [Download](https://www.paraview.org/download/) ParaView 5.11
- Create a virtual environment with `trame`
  ```bash
  python3.9 -m venv pvenv
  . pvenv/bin/activate
  pip install -U pip
  pip install trame
  ```

- For the `rock.py` example you will need to download the data using
  ```shell
  curl "https://www.paraview.org/files/ExternalData/SHA512/e6de2c81f7cfda82cdcad65da1a7a5c3817e8c6ced1366e95ab1593cc529e207ff33a9da3b1e4e280a5c4b6c4d5821a9167b90c5f9e8dcc1e07c64f0e1cfe594" --output rock.vti
  ```

- Use ParaView's `pvpython` interpreter to run any of the reference applications. For example,
  ```bash
  ParaView-5.11-x/bin/pvpython ./sync/python/wavelet.py --venv pvenv --port 8080
  ```
  or
  ```bash
  ParaView-5.11-x/bin/pvpython ./sync/python/wavelet.py --venv pvenv --port 8080 -D <path to directry containing rock.vti>
  ```


## Using Docker
For reproducibility and comparison with the async version you may also create
your own docker image. Two base images are provided. One based on nvidia EGL
that uses hardware accelerated rendering as well as a software-based rendering
using osmesa.

__NVIDIA EGL rendering__
You may need to setup `nvidia-docker2`. Follow instructions outlined [here](https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/install-guide.html).

To build the image execute:
```bash
$ ./sync/docker/nvidia/build.sh
```

To run the example execute:
```bash
$ ./sync/docker/nvidia/run-docker.sh ./sync/python/rock.py --port 8080
```

__OSMesa software rendering__

To build the image execute:
```bash
$ ./sync/docker/osmesa/build.sh
```


To run the example execute:
```bash
$ ./sync/docker/osmesa/run-docker.sh ./sync/python/rock.py --port 8080
```

## Tips
When running multiple containers, try to use different port numbers, 8080, 8081, etc.
