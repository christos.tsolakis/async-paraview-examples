#!/bin/sh

# setup a python virtual environment with trame
set -e

python -m venv /opt/paraview/runtime/.pvenv

. /opt/paraview/runtime/.pvenv/bin/activate

pip install -U pip
pip install trame
