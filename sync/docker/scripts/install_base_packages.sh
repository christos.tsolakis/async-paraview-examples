#!/bin/sh

export DEBIAN_FRONTEND="noninteractive"

apt-get update

# Configure time
apt-get install -y tzdata 
ln -fs /usr/share/zoneinfo/America/New_York /etc/localtime
dpkg-reconfigure --frontend noninteractive tzdata

# base packages 
apt-get install -y --no-install-recommends \
    ca-certificates \
    libgomp1 \
    libosmesa6 \
    curl \
    openssl

# Install python packages
apt-get install -y --no-install-recommends python3.9-dev python3.9-venv python3-pip python3-setuptools

# Set default python version
update-alternatives --install /usr/bin/python python /usr/bin/python3.9 1
