The docker runtime containers have everything you need to run `wavelet.py`
and `rock.py`

Here's a listing of `/opt/paraview`

```shell
/opt/paraview/
|-- apps
|   |-- rock.py
|   `-- wavelet.py
|-- data
|   `-- Testing
`-- runtime
    |-- .pvenv
    |-- bin
    |-- lib
    `-- share
```
